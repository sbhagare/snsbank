import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable({
  providedIn: 'root',
})
export class CaseService {
  baseUrl = environment.apiUrl;
  loancalUrl= environment.loanCalCulateUrl;
  errorMsg!: string;
  kycstatus:any;
  skipSNSBank:any;
  constructor(private http: HttpClient) {}

  //================ create new case ==================================
  createCase(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}`, data, {observe:'response'})
    .pipe(
      catchError(this.errorHandler)
    )
  }

 // ====================get all existing customer case==================
  getAllCases(): Observable<any> {
  return this.http.get(this.baseUrl,{observe:'response'});
  
  }
  // ====================get existing customer case By Id==================
  getCaseById(id: any): Observable<any> {
   return this.http.get(`${this.baseUrl}/${id}`, {observe:'response'})
   .pipe(
    catchError(this.errorHandler)
  )
   
  }
 
  // ====================get loan calculate==================
  createCalculateLaonById(id: any): Observable<any> {
     
       let newUrl = this.loancalUrl+'/'+id
       console.log(newUrl)
    return this.http.post(newUrl,{}, {observe:'response'})
    .pipe(
      catchError(this.errorHandler)
    )
   }

   // ====================get existing customer case By Id==================
   updateCaseById(id: any, data: any): Observable<any> {
  
    let contentHeader : HttpHeaders = new HttpHeaders();
      let etag: any =localStorage.getItem('etag')
      contentHeader  = contentHeader.append('If-Match', etag);
      return this.http.put(`${this.baseUrl}/${id}`, data, {headers:contentHeader , observe:'response'})
    .pipe(
      catchError(this.errorHandler)
    )
   
   }

   // ====================get existing customer case By Id==================
   updateChangeStateById(id: any, data: any): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    let etag: any =localStorage.getItem('etag')
    headers = headers.append('If-Match', etag);
    let actionID='pyChangeStage'
    return this.http.put(`${this.baseUrl}/${id}?actionID=pyChangeStage`, data, {headers, observe:'response'})
    .pipe(
      catchError(this.errorHandler)
    )
   
   }

  setKYCStatus(status:any){
    this.kycstatus=status;
  }
  getKYCstatus(){
    return this.kycstatus;
  }
  setsnsBankStatus(snsBank:any){
    this.skipSNSBank=snsBank;
  }
  getsnsBankStatus(){
    return this.skipSNSBank;
  }

   errorHandler(error: any) {
   
    // let errorMessage = '';
    // if(error.error instanceof ErrorEvent) {
     
    //   // Get client-side error
    //   errorMessage = error.error;
    // } else {
    //    // Get server-side error
    //   errorMessage = `Error Code: ${error.status}\nMessage: ${ error}`;
    // }
    //console.log(errorMessage);
    return throwError(error);
 }


}

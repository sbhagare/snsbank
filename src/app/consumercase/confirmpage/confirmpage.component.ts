import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CaseService } from '../services/case.service';

@Component({
  selector: 'app-confirmpage',
  templateUrl: './confirmpage.component.html',
  styleUrls: ['./confirmpage.component.css']
})
export class ConfirmpageComponent implements OnInit {

  constructor(private route: ActivatedRoute,private caseService: CaseService,) { }
 caseName: any;
 filterdata:any;
 statusofid:any;
 confirmationmessage:any;

  ngOnInit(): void {
   this.caseName = this.route.snapshot.params['caseId'];
   var status = this.caseService.getKYCstatus();
   console.log(status);
   var snsBankStatus = this.caseService.getsnsBankStatus();
   if(snsBankStatus == 'skipsnsBank'){
    if(status=='Green'){
      this.confirmationmessage = 'We will contact you with your once your application is approved';
     }
     else{
         this.confirmationmessage = 'is the reference number of your application. Request you to please use the reference number for all further correspondence. In order for us to process it and return with a decision we will request a credit report and connect with you if any additional documents are required.';
      }
   }
   else{
    if(status=='Green'){
      this.confirmationmessage = 'We will contact you with your SNS Advisor appointment once your application is approved';
     }
     else{
         this.confirmationmessage = 'is the reference number of your application. Request you to please use the reference number for all further correspondence. In order for us to process it and return with a decision we will request a credit report and connect with you if any additional documents are required.';
      }
   }
  }
  
}

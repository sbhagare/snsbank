import { ModuleWithProviders, NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const boostrapModule = [
  NgxSliderModule,
  NgxUsefulSwiperModule,
  SlickCarouselModule,
  NgbModule,
];

@NgModule({
   imports: [boostrapModule],
  exports: [boostrapModule],
})
export class BoostrapmoduleModule { }

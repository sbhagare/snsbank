import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/authservice/auth.service';
import { isToken } from 'typescript';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  hide=true
  signinFormGroup!: FormGroup;
  loginSubmitted!: boolean;
  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    
    ) { }

  ngOnInit(): void {
    this.loginFormValidation();
  }

 //====================== sign in form validation =================
 loginFormValidation() {
  this.signinFormGroup = this._formBuilder.group({
    username: ['john.s@gmail.com', [Validators.required]],
    password: ['rules', [Validators.required]],
  });
 }

 get loginFn() {
  return this.signinFormGroup.controls;
}

loginSubmitData() {

  this.loginSubmitted = true;
  if (this.signinFormGroup.get('username')?.value==="john.s@gmail.com" && this.signinFormGroup.get('password')?.value==="rules") {
    
    let user =   'Basic Q29uc3VtZXJfTGVuZGluZ19BUEk6UnVsZXNAMTIzNDU2' //'Basic Q0xfQVBJOnJ1bGVz'

    const data = {
     name:this.signinFormGroup.get('username')?.value,
     pass:this.signinFormGroup.get('password')?.value
    }
    let IsToken = {
      login:true
    }
      
    this.authService.sendToken(user)
    localStorage.setItem('users',JSON.stringify(data))
    this.router.navigate(['/consumer/dashboard'])
  } else {
    this.router.navigate(['/'])
    //console.log('employee_details', this.signinFormGroup.value);
  }
}


}

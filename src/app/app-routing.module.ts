import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './authentication/sign-in/sign-in.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  // { path: '', component: HomeComponent },
  { path: 'login', component: SignInComponent },
  {
    path: '',
    loadChildren: () =>
      import('./consumerlanding/consumerlanding.module').then(
        (m) => m.ConsumerlandingModule
      ),
     // canActivate: [AuthGuard],
  },
  {
    path: 'consumer',
    loadChildren: () =>
      import('./consumercase/consumercase.module').then(
        (m) => m.ConsumercaseModule
      ),
    canActivate: [AuthGuard],
  },
  { path: '**', component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

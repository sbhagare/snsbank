import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/authservice/auth.service';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(public authService: AuthService) {}

  ngOnInit(): void {}

  Images = [
    {
      src: 'https://loremflickr.com/g/600/400/paris',
      alt: 'Image 1',
    },
    {
      src: 'https://loremflickr.com/600/400/brazil,rio',
      alt: 'Image 2',
    },
    {
      src: 'https://loremflickr.com/600/400/paris,girl/all',
      alt: 'Image 3',
    },
    {
      src: 'https://loremflickr.com/600/400/brazil,rio',
      alt: 'Image 4',
    },
    {
      src: 'https://loremflickr.com/600/400/paris,girl/all',
      alt: 'Image 5',
    },
    {
      src: 'https://loremflickr.com/600/400/brazil,rio',
      alt: 'Image 6',
    },
  ];

  config: SwiperOptions = {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    spaceBetween: 30,
  };

  //carosal slider
  slides = [
    { img: 'https://dummyimage.com/350x150/423b42/fff' },
    { img: 'https://dummyimage.com/350x150/2a2b7a/fff' },
    { img: 'https://dummyimage.com/350x150/1a2b7a/fff' },
    { img: 'https://dummyimage.com/350x150/7a2b7a/fff' },
    { img: 'https://dummyimage.com/350x150/9a2b7a/fff' },
    { img: 'https://dummyimage.com/350x150/5a2b7a/fff' },
    { img: 'https://dummyimage.com/350x150/4a2b7a/fff' },
  ];
  slideConfig = { slidesToShow: 4, slidesToScroll: 4 };

  slickInit(e: any) {
    console.log('slick initialized');
  }

  breakpoint(e: any) {
    console.log('breakpoint');
  }

  afterChange(e: any) {
    console.log('afterChange');
  }

  beforeChange(e: any) {
    console.log('beforeChange');
  }
  //end carosal
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authservice/auth.service';

@Component({
  selector: 'app-consumerlanding',
  templateUrl: './consumerlanding.component.html',
  styleUrls: ['./consumerlanding.component.css']
})
export class ConsumerlandingComponent implements OnInit {
  
  userData: any;

  constructor(public authService: AuthService) { }
    
  ngOnInit(): void {
     
   let data: any =localStorage.getItem('users');
       this.userData=JSON.parse(data)
  }

logout() {
  localStorage.clear()
  this.authService.logout()
}


}

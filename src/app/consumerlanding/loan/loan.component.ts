import { Options } from '@angular-slider/ngx-slider';
import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/authservice/auth.service';
import { CaseService } from 'src/app/consumercase/services/case.service';
import { NotificationService } from 'src/app/notifications/notification.service';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css'],
})
export class LoanComponent implements OnInit {
  disabled!: boolean;
  isLinear = true;
  isCheckedApplicant = false;
  coAppliciantEmployeeDetails = false;
  coAppliciantFinancialDetails = false;
  firstFormGroup!: FormGroup;
  coapplicantFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;
  productForm!: FormGroup;
  propertyFormGroup!: FormGroup;
  loanFormGroup!: FormGroup;
  coapplicantSubmitted!: boolean;
  isCheckedApplicantData!: boolean;
  browwerdetailSubmitted!: boolean;
  employeedetailSubmitted!: boolean;

  quantityArrayAmount = [] as any;
  assetTotalSumAmount = 0;
  monthlyArrayAmount = [] as any;
  monthlyTotalSumAmount = 0;
  recurringArrayAmount = [] as any;
  recurringTotalSumAmount = 0;
  libertyArrayAmount = [] as any;
  libertyTotalSumAmount = 0;
  totalIncomeCumser = 0;
  totalIncomeCumserExp = 0;
  totalExpensiveCoApplicant = 0;
  saveBtn!: boolean;
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'before';
  coapplicant_assetTotalSumAmount = 0;
  coapplicant_monthlyTotalSumAmount = 0;
  coapplicant_recurringTotalSumAmount = 0;
  coapplicant_libertyTotalSumAmount = 0;
  totalIncomecoApplicant = 0;
  finnacialdetailSubmitted!: boolean;
  propertydetailSubmitted!: boolean;
  loandetailsSubmitted!: boolean;
  uploadFormGroup!: FormGroup;
  uploadDocumentSubmitted!: boolean;
  nxtBtnShow!: boolean;
  viewTemp!: boolean;
  assetsDatas: any;
  monthllyDatas: any;
  viewData: any;
  recurrDatas: any;
  liabilDatas: any;
  viewPropertyData: any;
  viewLoanData: any;

  constructor(
    private _formBuilder: FormBuilder,
     public datepipe: DatePipe,
     private caseService: CaseService,
     private notificationService: NotificationService,
     private route: ActivatedRoute,
     private router: Router,
     private authService: AuthService
     ) {}

  // ============== slider =====================
  value: number = 0;
  options: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 0 },
      { value: 5 },
      { value: 10 },
      { value: 15 },
      { value: 20 },
      { value: 25 },
      { value: 30 },
    ],
  };

  options1: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 0 },
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 },
      { value: 5 },
      { value: 6 },
      { value: 7 },
      { value: 8 },
      { value: 9 },
      { value: 10 },
      { value: 11 },
      { value: 12 },
    ],
  };

  options2: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 0 },
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 },
      { value: 5 },
    ],
  };

  // ============== slider =====================

  foods = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' },
  ];

  loanProspoes = [
    { value: 'BuildHouse', viewValue: 'Build a New House' },
    { value: 'RenovateHouse', viewValue: 'Renovate a House' },
    { value: 'BuyNewHouse', viewValue: 'Buy a New House' },
    { value: 'BuyanAppartment', viewValue: 'Buy an Appartment' },
    { value: 'RenovateanAppartment', viewValue: 'Renovate an Appartment' },
  ];
  loanProducts = [
    { value: 'MortgageCommitment', viewValue: 'Mortgage commitment' },
    { value: 'Mortgage20yrsVilla', viewValue: 'Mortgage 20yrs Villa' },
    {
      value: 'Mortgage20yrsAppartment',
      viewValue: 'Mortgage 20yrs Appartment',
    },
    {
      value: 'MortgageLongTermRealEstate',
      viewValue: 'Mortgage Long Term Real Estate',
    },
    { value: 'MortgageRural', viewValue: 'Mortgage Rural' },
  ];

  countries = [
    { code: 'VI', code3: 'VIR', name: 'USA', number: '850' },
    { code: 'WF', code3: 'WLF', name: 'England', number: '876' },
    { name: 'Australia' },
    { name: 'Sweden' },
    { name: 'india' },
  ];

  cities = [
    { value: 'London', viewValue: 'London' },
    { value: 'Melbourne', viewValue: 'Melbourne' },
    { value: 'Stockholm', viewValue: 'Stockholm' },
    { value: 'Blr', viewValue: 'Blr' },
  ];

  employees = [
    { value: 'Permanent Employee', viewValue: 'Permanent Employee' },
    { value: 'student', viewValue: 'Student' },
    { value: 'self-employed', viewValue: 'Self Employed' },
    { value: 'hourly-basis', viewValue: 'Hourly Basis' },
    { value: 'pensioner', viewValue: 'Pensioner' },
    { value: 'on-probation', viewValue: 'On Probation' },
  ];


  finacials = [
    { value: 'PR', viewValue: 'Property' },
    { value: 'SB', viewValue: 'Stocks & Bonds' },
    { value: 'OT', viewValue: 'Others' },
  ];
  sourcefinacials = [
    { value: 'RI', viewValue: 'Rental income' },
    {
      value: 'CG',
      viewValue: 'Capital gains from Stocks & Bonds',
    },
    { value: 'II', viewValue: 'Interest income' },
    { value: 'BI', viewValue: 'Busness income' },
    { value: 'O', viewValue: 'Others' },
  ];
  recurringfinacials = [
    {
      value: 'Average Household Expense',
      viewValue: 'Average HouseHold Expense',
    },
    { value: 'Rental Expense', viewValue: 'Rental Expense' },
    { value: 'Medical Expense', viewValue: 'Medical Expense' },
    { value: 'Other Expense', viewValue: 'Other Expense' },
  ];
  liabilitfinacials = [
    { value: 'Car Loan', viewValue: 'Car Loan' },
    { value: 'House Loan', viewValue: 'House Loan' },
    { value: 'Others', viewValue: 'Others' },
  ];

  properties = [
    { value: 'rental', viewValue: 'Rental' },
    { value: 'owned', viewValue: 'Owned' },
  ];

  documentTypes = [
    { value: 'legaldocuments', viewValue: 'LegalDocuments' },
    { value: 'identificationproof', viewValue: 'IdentificationProof' },
    { value: 'employmentcertificate', viewValue: 'EmploymentCertificate' },
    { value: 'salarystatement', viewValue: 'SalaryStatement' },
  ];

  selectItem(e: any) {
    if (e === true) {
      this.isCheckedApplicant = true;
      //this.saveBtn = true;
    } else {
      this.isCheckedApplicant = false;
      this.isCheckedApplicantData = false;
      this.coAppliciantEmployeeDetails = false;
      this.coAppliciantFinancialDetails = false;
      // this.saveBtn = false;
    }
  }

  currencySymbols = [
    { name: 'AUD', value: 'AUD' },
    { name: 'CAD', value: 'CAD' },
    { name: 'CHF', value: 'CHF' },
    { name: 'EUR', value: 'EUR' },
    { name: 'GBP', value: 'GBP' },
    { name: 'HKD', value: 'HKD' },
    { name: 'JPY', value: 'JPY' },
    { name: 'NZD', value: 'NZD' },
    { name: 'SEK', value: 'SEK' },
    { name: 'USD', value: 'USD' },
  ];

  ngOnInit(): void {
    //========borrow details form validation================
    this.firstFormGroup = this._formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      contactNumber: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      emailAddress: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
      countryName: ['', Validators.required],
      zipCode: [
        '',
        [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      ],
      cityName: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      isCoApplicantCked: [''],

      coapplicantfirstName: [''],
      coapplicantlastName: [''],
      coapplicantcontactNumber: [''],
      coapplicantemailAddress: [''],
      coapplicantcountryName: [''],
      coapplicantzipCode: [''],
      coapplicantcityName: [''],
      coapplicantaddressLine1: [''],
      coapplicantaddressLine2: [''],
    });
    //========employee details form validation================

    this.secondFormGroup = this._formBuilder.group({
      employeeType: ['', [Validators.required]],
      employerName: [
        '',
        [Validators.required, Validators.pattern('^[a-zA-Z]+$')],
      ],
      position: ['', [Validators.required]],
      countryName: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      cityName: ['', [Validators.required]],
      pinCode: [
        '',
        [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      ],
      contactNumber: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      addressLine1: ['', [Validators.required]],
      // coapplicant_employeeType: ['', [Validators.required]],
      // coapplicant_employerName: [
      //   '',
      //   [Validators.required, Validators.pattern('^[a-zA-Z]+$')],
      // ],
      // coapplicant_position: ['', [Validators.required]],
      // coapplicant_countryName: ['', [Validators.required]],
      // coapplicant_startDate: ['', [Validators.required]],
      // coapplicant_cityName: ['', [Validators.required]],
      // coapplicant_pinCode: [
      //   '',
      //   [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      // ],
      // coapplicant_contactNumber: [
      //   '',
      //   [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      // ],
      // coapplicant_addressLine1: ['', [Validators.required]],

      coapplicant_employeeType: [''],
      coapplicant_employerName: [''],
      coapplicant_position: [''],
      coapplicant_countryName: [''],
      coapplicant_startDate: [''],
      coapplicant_cityName: [''],
      coapplicant_pinCode: [''],
      coapplicant_contactNumber: [''],
      coapplicant_addressLine1: [''],
    });

    this.coapplicantFormGroup = this._formBuilder.group({
      ssnumber: [
        '',
        [
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/),
          Validators.pattern('.*\\S.*[a-zA-z0-9 ]'),
        ],
      ],
    });

    //for form array
    this.productForm = this._formBuilder.group({
      //  name: [{value: 'assets', disabled: true}],
      totalIncome: ['', [Validators.required]],
      quantities: this._formBuilder.array([]),
      monthlies: this._formBuilder.array([]),
      recurries: this._formBuilder.array([]),
      liabilities: this._formBuilder.array([]),
      // ==========co-applicant ============
      coapplicant_totalIncome: [0],
      coapplicant_quantities: this._formBuilder.array([]),
      coapplicant_monthlies: this._formBuilder.array([]),
      coapplicant_recurries: this._formBuilder.array([]),
      coapplicant_liabilities: this._formBuilder.array([]),
    });

    //================== Property FormGroup==================
    this.propertyFormGroup = this._formBuilder.group({
      countryName: ['', Validators.required],
      zipCode: [
        '',
        [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      ],
      cityName: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      propertyStatus: ['', [Validators.required]],
      propertyPrice: ['', [Validators.required]],
      propertyCurrency: ['', [Validators.required]],
      exsitingPropertyChecked: [''],
      existpropertyTpye: [''],
      existingpropertyContracts: [''],
      existingpropertyAddressLine1: [''],
      existingpropertyAddressLine2: [''],
      existingpropertyCountryName: [''],
      existingpropertyCityName: [''],
      existingpropertyZipCode: [''],
      existingpropertyPrice: [''],
      arrangePropertyChecked: [''],
      saleAmount: [''],
      saleCurrency: [''],
      transferDate: [''],
    });

    //================== Loan FormGroup==================
    this.loanFormGroup = this._formBuilder.group({
      loanPurpose: ['', [Validators.required]],
      loanProductType: ['', [Validators.required]],
      productScope: 'Shard',
      checkMortgage: [''],
      mortgageLoanAmount: [''],
      mortgageLoanCurrency: [''],
      morgtSelfAmount: [''],
      morgtSelfCurrency: [''],
      mortgageTimePeriod: [''],
      checkBridge: [''],
      bridgeLoanAmount: [''],
      bridgeLoanCurrency: [''],
      bridgeLoanDate: [''],
      bridgeTimePeriodinMonth: [''],
      checkAreyouInterested: [''],
      downpaymentAmount: [''],
      downpaymentCurrency: [''],
      downpaymentDate: [''],
      downpaymentTimePeriodinYear: [''],
      aggred: [''],
    });

    //================= Upload Document Details =================
    //for form array
    this.uploadFormGroup = this._formBuilder.group({
      //  name: [{value: 'assets', disabled: true}],
      uploadDocumetRowItems: this._formBuilder.array([]),
    });
  }




  get coapplicant() {
    return this.coapplicantFormGroup.controls;
  }
  get personaldetil() {
    return this.firstFormGroup.controls;
  }
  get employeedetil() {
    return this.secondFormGroup.controls;
  }
  get finnacialdetil() {
    return this.productForm.controls;
  }
  get propertydetil() {
    return this.propertyFormGroup.controls;
  }
  get loandetil() {
    return this.loanFormGroup.controls;
  }
  get documentdetil() {
    return this.uploadFormGroup.controls;
  }

  // ===========================Start Borrower Details ===============================

  getCoApplicantData() {
    this.coapplicantSubmitted = true;
    if (this.coapplicantFormGroup.invalid) {
      this.isCheckedApplicantData = false;
      this.coAppliciantEmployeeDetails = false;
      this.coAppliciantFinancialDetails = false;
      return;
    } else {
      this.isCheckedApplicantData = true;
      this.saveBtn = false;
      this.coAppliciantEmployeeDetails = true;
      this.coAppliciantFinancialDetails = true;

      console.log('get_co-applicatnt', this.coapplicantFormGroup.value);
    }
  }

  borrowerDetailsData() {
    this.browwerdetailSubmitted = true;
    if (this.firstFormGroup.invalid) {
      return;
    } else {
      let postData = this.createcaseData()
      let updatedataVal = this.reqformDatafun(
        this.firstFormGroup.value,
        this.secondFormGroup.value,
        this.propertyFormGroup.value,
        this.productForm.value,
        this.loanFormGroup.value
      );
      console.log('borrow_details', updatedataVal);
      this.caseService.createCase(postData).subscribe((data)=>{
        console.log('case_data',data)
        if (data.status===201) {
          
          let caseTd = data.body.ID
          localStorage.removeItem('etag')
          localStorage.setItem('etag', data.headers.get('etag'));
          this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
            (updata) => {
              console.log('cretaed_Cass', updata);
             if (updata.status===204) {
               
              let str = caseTd;
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              this.notificationService.showSuccess(
                'Case Created successfully',
                `${caseName}`
              );
              // this.router.navigateByUrl('/consumer/dashboard');

             }
            },
            (error) => {
              console.log('create_data_error', error);
              let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
              this.notificationService.showError(
                `${errorMessage}`,
                'Server error occurs'
              );
            }
          );

        }

      },
      (error)=>{
        this.notificationService.showError(`${error.message}`,'Server error occurs')
      }
      )
      
    }
  }

  // =========================== End Borrower Details ===============================

  // =========================== start Employee Details ===============================

  employeeDetailsData() {
    this.employeedetailSubmitted = true;
    if (this.secondFormGroup.invalid) {
      return;
    } else {
     
      let postData = this.createcaseData()
      let updatedataVal = this.reqformDatafun(
        this.firstFormGroup.value,
        this.secondFormGroup.value,
        this.propertyFormGroup.value,
        this.productForm.value,
        this.loanFormGroup.value
      );
      console.log('employee_details', updatedataVal);
      this.caseService.createCase(postData).subscribe((data)=>{
        console.log('case_data',data)
        if (data.status===201) {
          
          let caseTd = data.body.ID
          localStorage.removeItem('etag')
          localStorage.setItem('etag', data.headers.get('etag'));
          this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
            (updata) => {
              console.log('cretaed_Cass', updata);
             if (updata.status===204) {
               
              let str = caseTd;
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              this.notificationService.showSuccess(
                'Case Created successfully',
                `${caseName}`
              );
              // this.router.navigateByUrl('/consumer/dashboard');

             }
            },
            (error) => {
              console.log('create_data_error', error);
              let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
              this.notificationService.showError(
                `${errorMessage}`,
                'Server error occurs'
              );
            }
          );

        }

      },
      (error)=>{
        this.notificationService.showError(`${error.message}`,'Server error occurs')
      }
      )

    }
  }
  // =========================== End Employee Details ===============================

  //=========== Start Finnacial Details =======================
  quantities(): FormArray {
    return this.productForm.get('quantities') as FormArray;
  }
  monthlies(): FormArray {
    return this.productForm.get('monthlies') as FormArray;
  }
  recurries(): FormArray {
    return this.productForm.get('recurries') as FormArray;
  }
  liabilities(): FormArray {
    return this.productForm.get('liabilities') as FormArray;
  }

  newQuantity(): FormGroup {
    return this._formBuilder.group({
      AssetType: '',
      OriginalValue: '',
      CurrencyCodesList: '',
    });
  }
  newMonthly(): FormGroup {
    return this._formBuilder.group({
      IncomeType: '',
      IncomeValue: '',
      CurrencyCodesList: '',
    });
  }
  newRecurring(): FormGroup {
    return this._formBuilder.group({
      ExpenseType: '',
      ExpenseAmount: '',
      CurrencyCodesList: '',
    });
  }
  newLiberity(): FormGroup {
    return this._formBuilder.group({
      LiabilityType: '',
      LiabilityAmount: '',
      CurrencyCodesList: '',
    });
  }

  addCommonQuantity(val: any) {
    if (val === 'month') {
      this.monthlies().push(this.newMonthly());
    } else if (val === 'recur') {
      this.recurries().push(this.newRecurring());
    } else if (val === 'liabilities') {
      this.liabilities().push(this.newLiberity());
    } else {
      this.quantities().push(this.newQuantity());
    }
  }

  commonRemoveQuantity(i: number, val: any) {
    if (val === 'assets') {
      this.quantities().removeAt(i);
      let qutit = this.productForm.get('quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.assetTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'monthly') {
      this.monthlies().removeAt(i);
      let month = this.productForm.get('monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.monthlyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'recurries') {
      this.recurries().removeAt(i);
      let recurr = this.productForm.get('recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.recurringTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else {
      this.liabilities().removeAt(i);
      let liabilit = this.productForm.get('liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.libertyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    }
    this.totalIncomeCumser =
      this.assetTotalSumAmount +
      this.monthlyTotalSumAmount +
      this.recurringTotalSumAmount;
    this.totalIncomeCumserExp = this.libertyTotalSumAmount;
  }

  onKeypressEvent(event: any, i: number, val: any) {
    if (val === 'assets') {
      let qutit = this.productForm.get('quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.assetTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'month') {
      let month = this.productForm.get('monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.monthlyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'recur') {
      let recurr = this.productForm.get('recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.recurringTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else {
      let liabilit = this.productForm.get('liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.libertyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    }

    this.totalIncomeCumser =
      this.assetTotalSumAmount +
      this.monthlyTotalSumAmount +
      this.recurringTotalSumAmount;
    this.totalIncomeCumserExp = this.libertyTotalSumAmount;
  }

  //================ co-applicant ================

  coapplicant_quantities(): FormArray {
    return this.productForm.get('coapplicant_quantities') as FormArray;
  }
  coapplicant_monthlies(): FormArray {
    return this.productForm.get('coapplicant_monthlies') as FormArray;
  }
  coapplicant_recurries(): FormArray {
    return this.productForm.get('coapplicant_recurries') as FormArray;
  }
  coapplicant_liabilities(): FormArray {
    return this.productForm.get('coapplicant_liabilities') as FormArray;
  }

  newcoapplicant_Quantity(): FormGroup {
    return this._formBuilder.group({
      AssetType: '',
      OriginalValue: '',
      CurrencyCodesList: '',
    });
  }
  newcoapplicant_Monthly(): FormGroup {
    return this._formBuilder.group({
      IncomeType: '',
      IncomeValue: '',
      CurrencyCodesList: '',
    });
  }
  newcoapplicant_Recurring(): FormGroup {
    return this._formBuilder.group({
      ExpenseType: '',
      ExpenseAmount: '',
      CurrencyCodesList: '',
    });
  }
  newcoapplicant_Liberity(): FormGroup {
    return this._formBuilder.group({
      LiabilityType: '',
      LiabilityAmount: '',
      CurrencyCodesList: '',
    });
  }

  coapplicant_addCommonQuantity(val: any) {
    if (val === 'month') {
      this.coapplicant_monthlies().push(this.newcoapplicant_Monthly());
    } else if (val === 'recur') {
      this.coapplicant_recurries().push(this.newcoapplicant_Recurring());
    } else if (val === 'liabilities') {
      this.coapplicant_liabilities().push(this.newcoapplicant_Liberity());
    } else {
      this.coapplicant_quantities().push(this.newcoapplicant_Quantity());
    }
  }

  coapplicant_commonRemoveQuantity(i: number, val: any) {
    if (val === 'assets') {
      this.coapplicant_quantities().removeAt(i);
      let qutit = this.productForm.get('coapplicant_quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.coapplicant_assetTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'monthly') {
      this.coapplicant_monthlies().removeAt(i);
      let month = this.productForm.get('coapplicant_monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.coapplicant_monthlyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'recurries') {
      this.coapplicant_recurries().removeAt(i);
      let recurr = this.productForm.get('coapplicant_recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.coapplicant_recurringTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else {
      this.coapplicant_liabilities().removeAt(i);
      let liabilit = this.productForm.get('coapplicant_liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.coapplicant_libertyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    }

    this.totalIncomecoApplicant =
      this.coapplicant_assetTotalSumAmount +
      this.coapplicant_monthlyTotalSumAmount +
      this.coapplicant_recurringTotalSumAmount;
    this.totalExpensiveCoApplicant = this.coapplicant_libertyTotalSumAmount;
  }

  coapplicant_onKeypressEvent(event: any, i: number, val: any) {
    if (val === 'assets') {
      let qutit = this.productForm.get('coapplicant_quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.coapplicant_assetTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
      console.log(this.coapplicant_assetTotalSumAmount);
    } else if (val === 'month') {
      let month = this.productForm.get('coapplicant_monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.coapplicant_monthlyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'recur') {
      let recurr = this.productForm.get('coapplicant_recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.coapplicant_recurringTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else {
      let liabilit = this.productForm.get('coapplicant_liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.coapplicant_libertyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    }
    this.totalIncomecoApplicant =
      this.coapplicant_assetTotalSumAmount +
      this.coapplicant_monthlyTotalSumAmount +
      this.coapplicant_recurringTotalSumAmount;
    this.totalExpensiveCoApplicant = this.coapplicant_libertyTotalSumAmount;
  }

  sym = '';
  symblChange(event: any) {
    this.sym = event.target.value;
    console.log(event.target.value);
  }
  coapplicant_symblChange(event: any) {
    console.log(event.target.value);
  }

  cst = 0;
  ccst = 0;
  onBlur(event: any, val: any) {
    this.cst =
      this.productForm.get('totalIncome')?.value.length > 0
        ? parseFloat(this.productForm.get('totalIncome')?.value)
        : 0;
    this.ccst =
      this.productForm.get('coapplicant_totalIncome')?.value.length > 0
        ? parseFloat(this.productForm.get('coapplicant_totalIncome')?.value)
        : 0;
  }

  onSubmit() {
    this.finnacialdetailSubmitted = true;
    if (this.productForm.invalid) {
      return;
    } else {
      let postData = this.createcaseData()
      let updatedataVal = this.reqformDatafun(
        this.firstFormGroup.value,
        this.secondFormGroup.value,
        this.propertyFormGroup.value,
        this.productForm.value,
        this.loanFormGroup.value
      );
      console.log('financial_details', updatedataVal);
      this.caseService.createCase(postData).subscribe((data)=>{
        console.log('case_data',data)
        if (data.status===201) {
          
          let caseTd = data.body.ID
          localStorage.removeItem('etag')
          localStorage.setItem('etag', data.headers.get('etag'));
          this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
            (updata) => {
              console.log('cretaed_Cass', updata);
             if (updata.status===204) {
               
              let str = caseTd;
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              this.notificationService.showSuccess(
                'Case Created successfully',
                `${caseName}`
              );
              // this.router.navigateByUrl('/consumer/dashboard');

             }
            },
            (error) => {
              console.log('create_data_error', error);
              let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
              this.notificationService.showError(
                `${errorMessage}`,
                'Server error occurs'
              );
            }
          );

        }

      },
      (error)=>{
        this.notificationService.showError(`${error.message}`,'Server error occurs')
      }
      )
    }
  }

  //=========== End Finnacial Details =======================

  //======================== Property Details ==========================
  existpropertyDetailsData = false;
  arrangepropertyDetailsData = false;
  selectexsitingPropertyChecked(e: any) {
    console.log(e);
    if (e === true) {
      this.existpropertyDetailsData = true;
    } else {
      this.existpropertyDetailsData = false;
    }
  }
  selectarrangePropertyChecked(e: any) {
    console.log(e);
    if (e === true) {
      this.arrangepropertyDetailsData = true;
    } else {
      this.arrangepropertyDetailsData = false;
    }
  }

  onSubmitPropertyData() {
    this.propertydetailSubmitted = true;
    if (this.propertyFormGroup.invalid) {
      return;
    } else {

      let postData = this.createcaseData()
      let updatedataVal = this.reqformDatafun(
        this.firstFormGroup.value,
        this.secondFormGroup.value,
        this.propertyFormGroup.value,
        this.productForm.value,
        this.loanFormGroup.value
      );
      console.log('property_details', updatedataVal);
      this.caseService.createCase(postData).subscribe((data)=>{
        console.log('case_data',data)
        if (data.status===201) {
          
          let caseTd = data.body.ID
          localStorage.removeItem('etag')
          localStorage.setItem('etag', data.headers.get('etag'));
          this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
            (updata) => {
              console.log('cretaed_Cass', updata);
             if (updata.status===204) {
               
              let str = caseTd;
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              this.notificationService.showSuccess(
                'Case Created successfully',
                `${caseName}`
              );
              // this.router.navigateByUrl('/consumer/dashboard');

             }
            },
            (error) => {
              console.log('create_data_error', error);
              let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
              this.notificationService.showError(
                `${errorMessage}`,
                'Server error occurs'
              );
            }
          );

        }

      },
      (error)=>{
        this.notificationService.showError(`${error.message}`,'Server error occurs')
      }
      )


    }
  }

  // ======================= End Property Details =======================

  //==========================Loan Details =====================
  mortgageChecked = false;
  bridgeChecked = false;
  downloadpaymentChecked = false;

  selectMortgage(e: any) {
    if (e === true) {
      this.mortgageChecked = true;
    } else {
      this.mortgageChecked = false;
    }
  }

  selectBridge(e: any) {
    if (e === true) {
      this.bridgeChecked = true;
    } else {
      this.bridgeChecked = false;
    }
  }

  selectDownPayment(e: any) {
    if (e === true) {
      this.downloadpaymentChecked = true;
    } else {
      this.downloadpaymentChecked = false;
    }
  }

  onSubmitLoanData() {
    this.loandetailsSubmitted = true;
    if (this.loanFormGroup.invalid) {
      return;
    } else {

        let postData = this.createcaseData()
       let updatedataVal = this.reqformDatafun(
        this.firstFormGroup.value,
        this.secondFormGroup.value,
        this.propertyFormGroup.value,
        this.productForm.value,
        this.loanFormGroup.value
      );
      console.log('borrow_details', updatedataVal);
      this.caseService.createCase(postData).subscribe((data)=>{
        console.log('case_data',data)
        if (data.status===201) {
          
          let caseTd = data.body.ID
          localStorage.removeItem('etag')
          localStorage.setItem('etag', data.headers.get('etag'));
          this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
            (updata) => {
              console.log('cretaed_Cass', updata);
             if (updata.status===204) {
               
              let str = caseTd;
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              this.notificationService.showSuccess(
                'Case Created successfully',
                `${caseName}`
              );
              // this.router.navigateByUrl('/consumer/dashboard');

             }
            },
            (error) => {
              console.log('create_data_error', error);
              let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
              this.notificationService.showError(
                `${errorMessage}`,
                'Server error occurs'
              );
            }
          );

        }

      },
      (error)=>{
        this.notificationService.showError(`${error.message}`,'Server error occurs')
      }
      )
    }
  }

  // ======================= End Loan Details =======================

  //===================Upload Document Details=================================
  myFiles: string[] = [];
  currentDateTime = [] as any;

  uploadDocumetRowItems(): FormArray {
    return this.uploadFormGroup.get('uploadDocumetRowItems') as FormArray;
  }

  newUploadDocument(): FormGroup {
    return this._formBuilder.group({
      documents: ['', [Validators.required]],
      uploadFile: ['', [Validators.required]],
      uploadedBy: '',
      dateTime: '',
    });
  }

  onFileChange(event: any, j: number) {
    this.currentDateTime[j] = this.datepipe.transform(
      new Date(),
      'MM/dd/yyyy h:mm:ss'
    );

    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
  }

  addFileUploadField() {
    this.uploadDocumetRowItems().push(this.newUploadDocument());
  }

  removeUploadFile(i: number) {
    this.uploadDocumetRowItems().removeAt(i);
  }

  onSubmitDocumentUploadData() {
    this.uploadDocumentSubmitted = true;
    if (
      this.uploadFormGroup.invalid ||
      this.uploadFormGroup.get('uploadDocumetRowItems')?.value.length === 0
    ) {
      this.uploadDocumentSubmitted = true;
      return;
    } else {
       this.uploadDocumentSubmitted = false;
      // console.log(
      //   this.uploadFormGroup.get('uploadDocumetRowItems')?.value.length
      // );
         

      let postData = this.createcaseData()
      let updatedataVal = this.reqformDatafun(
       this.firstFormGroup.value,
       this.secondFormGroup.value,
       this.propertyFormGroup.value,
       this.productForm.value,
       this.loanFormGroup.value
     );
     console.log('borrow_details', updatedataVal);
     this.caseService.createCase(postData).subscribe((data)=>{
       console.log('case_data',data)
       if (data.status===201) {
         
         let caseTd = data.body.ID
         localStorage.removeItem('etag')
         localStorage.setItem('etag', data.headers.get('etag'));
         this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
           (updata) => {
             console.log('cretaed_Cass', updata);
            if (updata.status===204) {
              
              this.caseService.getCaseById(caseTd).subscribe(
                (data) => {
                      console.log(data)
                   this.viewTemp = true
                   this.viewData = data.body.content.pyWorkParty;
                     this.assetsDatas = data.body.content.pyWorkParty.Customer.Assets;
                       this.monthllyDatas = data.body.content.pyWorkParty.Customer.IncomeList;
                      this.recurrDatas = data.body.content.pyWorkParty.Customer.ExpensesList;
                      this.liabilDatas = data.body.content.pyWorkParty.Customer.LiabilitiesList;
                     this.viewLoanData = data.body.content;
                     this.viewPropertyData = data.body.content.PropertyDetails;
                    console.log(typeof  this.viewPropertyData)
                   
                  },
                  (error) => {
                    console.log(error);
                    this.notificationService.showError(
                      `${error.message}`,
                      'error Occuring'
                    );
                  }
                );



             let str = caseTd;
             let caseName = str.substring(str.lastIndexOf(' ') + 1);
             this.notificationService.showSuccess(
               'Case Created successfully',
               `${caseName}`
             );
             this.nxtBtnShow = true
             // this.router.navigateByUrl('/consumer/dashboard');

            }
           },
           (error) => {
             console.log('create_data_error', error);
             let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
             this.notificationService.showError(
               `${errorMessage}`,
               'Server error occurs'
             );
           }
         );

       }

     },
     (error)=>{
       this.notificationService.showError(`${error.message}`,'Server error occurs')
     }
     )




    }
  }

  //=================== End Upload Document Details=================================

  //==================== start Review Details========================================

  onSubmitReviewdData(){

    let postData = this.createcaseData()
    let updatedataVal = this.reqformDatafun(
     this.firstFormGroup.value,
     this.secondFormGroup.value,
     this.propertyFormGroup.value,
     this.productForm.value,
     this.loanFormGroup.value
   );
   console.log('borrow_details', updatedataVal);
   this.caseService.createCase(postData).subscribe((data)=>{
     console.log('case_data',data)
     if (data.status===201) {
       
       let caseTd = data.body.ID
       localStorage.removeItem('etag')
       localStorage.setItem('etag', data.headers.get('etag'));
       this.caseService.updateCaseById(caseTd, updatedataVal).subscribe(
         (updata) => {
           console.log('cretaed_Cass', updata);
          if (updata.status===204) {
           let str = caseTd;
           let caseName = str.substring(str.lastIndexOf(' ') + 1);
            this.caseService.createCalculateLaonById(caseTd).subscribe(
              (calResp)=>{
                if (calResp.status===204) {
                  localStorage.removeItem('etag')
                  localStorage.setItem('etag', calResp.headers.get('etag'));
                  this.caseService.updateChangeStateById(caseTd, this.changeStateData()).subscribe(
                    (changeResp)=>{
                     if (changeResp.status===204) {
                         this.notificationService.showSuccess(
                        'Case Created successfully',
                       `${caseName}`
                       );
                       this.router.navigateByUrl(`/consumer/confirm/${caseName}`);
                     }
                    }
                  )

                }
     
              }
            )

          }
         },
         (error) => {
           console.log('create_data_error', error);
           let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
           this.notificationService.showError(
             `${errorMessage}`,
             'Server error occurs'
           );
         }
       );

     }

   },
   (error)=>{
     this.notificationService.showError(`${error.message}`,'Server error occurs')
   }
   )

  }

  //======================= End Review Detials ==================================

   //========================== Normal date Convert from Response (12/18/2021)===========================================
  private formatDate(date: any) {
    console.log(date);
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

 //========================== Date convert from IST to GMT===========================================
  private formatDateGMT(date: any) {
    if (date === '') {
      return '';
    } else {
      const d = new Date(date);

      //console.log(d.toUTCString())
      let month = '' + (d.getMonth() + 1);
      let day = '' + d.getDate();
      const year = d.getFullYear();
      let datetext = 'T000000.000 GMT';
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return year + month + day + datetext;
    }
  }

//============================== request form data formatting here=========================
  reqformDatafun(
    borrowdata: any,
    empData: any,
    propData: any,
    finanData: any,
    loanData: any
  ) {
    console.log('empData ', empData)
    //================if co applicant check box is checked then co-applianct's borrower data=====================
    let coappCheckFirstName =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantfirstName
        : '';
    let coappCheckLastName =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantlastName
        : '';
    let coappCheckContactNumber =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantcontactNumber
        : '';
    let coappCheckEmailAddress =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantemailAddress
        : '';
    let coappCheckAddressLine1 =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantaddressLine1
        : '';
    let coappCheckAddressLine2 =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantaddressLine2
        : '';
    let coappCheckCityName =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantcityName
        : '';
    let coappCheckCountryName =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantcountryName
        : '';
    let coappCheckZipCode =
      borrowdata.isCoApplicantCked === true
        ? borrowdata.coapplicantzipCode
        : '';

    //======================== if co applicant check box is checked then co-applianct's employee data===================

    let coappCheckEmployType =
      borrowdata.isCoApplicantCked === true
        ? empData.coapplicant_employeeType
        : '';
    let coappCheckEmployerName =
      borrowdata.isCoApplicantCked === true
        ? empData.coapplicant_employerName
        : '';
    let coappCheckEmployPosition =
      borrowdata.isCoApplicantCked === true ? empData.coapplicant_position : '';
    let coappCheckEmployStartDate =
      borrowdata.isCoApplicantCked === true
        ? this.formatDateGMT(empData.coapplicant_startDate)
        : '';
    let coappCheckEmployCityName =
      borrowdata.isCoApplicantCked === true ? empData.coapplicant_cityName : '';
    let coappCheckEmployCountryName =
      borrowdata.isCoApplicantCked === true
        ? empData.coapplicant_countryName
        : '';
    let coappCheckEmployPincode =
      borrowdata.isCoApplicantCked === true ? empData.coapplicant_pinCode : '';
    let coappCheckEmployAddresLine1 =
      borrowdata.isCoApplicantCked === true
        ? empData.coapplicant_addressLine1
        : '';
    let coappCheckEmployContactNumber =
      borrowdata.isCoApplicantCked === true
        ? empData.coapplicant_contactNumber
        : '';

    //====================== if co applicant check box is checked then co-applianct's financial data ============================
    let coapplicantFinancialbaseIncome =
      borrowdata.isCoApplicantCked === true
        ? finanData.coapplicant_totalIncome
        : 0;
    let coapplicantFinancialAssets =
      borrowdata.isCoApplicantCked === true
        ? finanData.coapplicant_quantities
        : [];
    let coapplicantFinancialMonthlySource =
      borrowdata.isCoApplicantCked === true
        ? finanData.coapplicant_monthlies
        : [];
    let coapplicantFinancialRecurring =
      borrowdata.isCoApplicantCked === true
        ? finanData.coapplicant_recurries
        : [];
    let coapplicantFinancialLiabilities =
      borrowdata.isCoApplicantCked === true
        ? finanData.coapplicant_liabilities
        : [];

    // ===========================if existing property check box is checked ==============================
    let existpropertyTpye =
      propData.exsitingPropertyChecked === true ? 'SFR' : '';
    let existingpropertyContracts =
      propData.exsitingPropertyChecked === true ? 'Yes' : 'No';
    let existingpropertyAddressLine1 =
      propData.exsitingPropertyChecked === true ? 'Tomelilla, Sverige' : '';
    let existingpropertyAddressLine2 =
      propData.exsitingPropertyChecked === true ? 'SE-273 94' : '';
    let existingpropertyCountryName =
      propData.exsitingPropertyChecked === true ? 'Sweden' : '';
    let existingpropertyCityName =
      propData.exsitingPropertyChecked === true ? 'Stockholm' : '';
    let existingpropertyZipCode =
      propData.exsitingPropertyChecked === true ? '273944' : '';
    let existingpropertyPrice =
      propData.exsitingPropertyChecked === true ? '504567' : '';

    // ===========================if existing sale arrange property check box is  checked  ==============================
    let saleAmount =
      propData.arrangePropertyChecked === true ? propData.saleAmount : 0;
    let saleCurrency =
      propData.arrangePropertyChecked === true ? propData.saleCurrency : '';
    let transferDate =
      propData.arrangePropertyChecked === true
        ? this.formatDateGMT(propData.transferDate)
        : '';

    // ===========================if Mortgate loan check box is checked  ==============================
    let mortgageLoanAmount =
      loanData.checkMortgage === true ? loanData.mortgageLoanAmount : 0;
    let mortgageLoanCurrency =
      loanData.checkMortgage === true ? loanData.mortgageLoanCurrency : '';
    let morgtSelfAmount =
      loanData.checkMortgage === true ? loanData.morgtSelfAmount : 0;
    let morgtSelfCurrency =
      loanData.checkMortgage === true ? loanData.morgtSelfCurrency : '';
    let mortgageTimePeriod =
      loanData.checkMortgage === true ? loanData.mortgageTimePeriod : 0;

    // ===========================if Bridge loan check box is checked  ==============================
    let bridgeLoanAmount =
      loanData.checkBridge === true ? loanData.bridgeLoanAmount : 0;
    let bridgeLoanCurrency =
      loanData.checkBridge === true ? loanData.bridgeLoanCurrency : '';
    let bridgeLoanDate =
      loanData.checkBridge === true ? loanData.bridgeLoanDate : '';
    let bridgeTimePeriodinMonth =
      loanData.checkBridge === true ? loanData.bridgeTimePeriodinMonth : 0;

    // ===========================if down payment check box is checked  ==============================
    let downpaymentAmount =
      loanData.checkAreyouInterested === true ? loanData.downpaymentAmount : 0;
    let downpaymentCurrency =
      loanData.checkAreyouInterested === true
        ? loanData.downpaymentCurrency
        : '';
    let downpaymentDate =
      loanData.checkAreyouInterested === true
        ? this.formatDateGMT(loanData.downpaymentDate)
        : '';
    let downpaymentTimePeriodinYear =
      loanData.checkAreyouInterested === true
        ? loanData.downpaymentTimePeriodinYear
        : 0;


     // ==================calculation total liabilities, Recurring, Source Income=======================
   function sumOfCustomerLiabilitAndCoapplicantLiabilit(datat: any) {
    let valueInBeginning = 0;
   let sumOfLiabilities = datat.reduce(
     (accumVariable:any, curValue: { LiabilityAmount: string; }) => accumVariable + parseFloat(curValue.LiabilityAmount)
     , valueInBeginning
     )
    return sumOfLiabilities
 }
 function sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(datat: any) {
   let valueInBeginning = 0;
  let sumOfLiabilities = datat.reduce(
    (accumVariable:any, curValue: { IncomeValue: string; }) => accumVariable + parseFloat(curValue.IncomeValue)
    , valueInBeginning
    )
   return sumOfLiabilities
}
function sumOfCustomerRecurringCostAndCoapplicantRecurringCost(datat: any) {
 let valueInBeginning = 0;
let sumOfLiabilities = datat.reduce(
  (accumVariable:any, curValue: { ExpenseAmount: string; }) => accumVariable + parseFloat(curValue.ExpenseAmount)
  , valueInBeginning
  )
 return sumOfLiabilities
}


    let formatData = {
      content: {
        CoApplicantCheck: borrowdata.isCoApplicantCked, // borrower page co-applicant check box
        CreditCheckPermit: loanData.aggred, // loan page  creadit check box
        Currency: 'INR',
        CustomerStatus: 'NewCustomer',
        ExistingCustomerRadio: 'Yes',
        IsExistingCustomer: 'true',
        IsManualHandlingDone: 'false',
        KYCColour: 'Green',
        LoanToValue: '0.00',
        SearchCustomerId: '890102-3287',
        SelectedCustomerCifNbr: '8901023289',
        SFFlag: 'true',
        TempDebtToIncome: '0.0387331487591052', //((.TotalExpense + .LoanInfo.EMIForBridgeLoan + .LoanInfo.EMIForDownPaymentLoan + .LoanInfo.EMIForMortgageLoan)/(.TotalIncome))
        TotalExpense:sumOfCustomerRecurringCostAndCoapplicantRecurringCost(finanData.recurries) + sumOfCustomerRecurringCostAndCoapplicantRecurringCost(finanData.coapplicant_recurries) + sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.liabilities) + sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.coapplicant_liabilities), //TotalRunningCost + TotalLiabilities
       TotalIncome:finanData.totalIncome + coapplicantFinancialbaseIncome + sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(finanData.monthlies) + sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(finanData.coapplicant_monthlies), //TotalBaseIncome + TotalOtherSourceOfIncome
        TotalBaseIncome: finanData.totalIncome + coapplicantFinancialbaseIncome, // sum of customer Income.BaseEmplIncome + sum of co-applicant Income.BaseEmplIncome
        TotalLiabilities: sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.liabilities) + sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.coapplicant_liabilities), //sum of customer LiabilitiesList.liabilitiesAmount + sum of co-applicant LiabilitiesList.liabilitiesAmount
        TotalOtherSourceOfIncome: sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(finanData.monthlies) + sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(finanData.coapplicant_monthlies), //sum of customer IncomeList.IncomeValue + sum of co-applicant IncomeList.IncomeValue 
        TotalRunningCost: sumOfCustomerRecurringCostAndCoapplicantRecurringCost(finanData.recurries) + sumOfCustomerRecurringCostAndCoapplicantRecurringCost(finanData.coapplicant_recurries), //sum of customer ExpensesList.ExpenseAmount + sum of co-applicant ExpensesList.ExpenseAmount
        CreditDetails: {},
        ExistingProperty: {
          CollateralPrice: '378425.25',
          CurrencyCodesList: saleCurrency,
          TransferDate: transferDate,
          IsSold: propData.arrangePropertyChecked,
          PropertyPrice: existingpropertyPrice,
          PropertyType: existpropertyTpye,
          SalesAmount: saleAmount,
          SignedContractForSale: existingpropertyContracts,
          Address: {
            AddressLine1: existingpropertyAddressLine1,
            AddressLine2: existingpropertyAddressLine2,
            City: existingpropertyCityName,
            Country: existingpropertyCountryName,
            ZipCode: existingpropertyZipCode,
          },
        },
        LoanInfo: {
          BridgeLoanAmtCurrency: bridgeLoanCurrency, // bridge loan currency
          BridgeLoanDate: this.formatDateGMT(bridgeLoanDate), // bridge loan date
          BridgeLoanPeriod: bridgeTimePeriodinMonth, // bridge Loan time period in months
          DownPaymentAmtCurrency: downpaymentCurrency, // down payment currency
          DownPaymentDate: downpaymentDate, // down payment date
          DownPaymentLoanPeriod: downpaymentTimePeriodinYear, // down payment time period in yrs
          DownPaymentRequired: loanData.checkAreyouInterested, // down payment check box required
         // EMIForBridgeLoan: '0.4532533232640007',
         // EMIForMortgageLoan: '0.0000',
          EndFinancingLoanPeriod: mortgageTimePeriod, // Mortgage Loan time period in yrs
         // EndFinancingLoanPeriodInitial: '5',
          EndFinancingRequired: loanData.checkMortgage, //Mortgage Loan required check box
         // InterestRateForBridgeLoan: '4',
          //InterestRateForDownPaymentLoan: '5.0',
          //InterestRateForMortgageLoan: '5',
          isBridgeLoanRequired: loanData.checkBridge, // bridge loan required check box
          LoanAmtCurrency: mortgageLoanCurrency, // mortage loan currency
          SelfContiCurrency: morgtSelfCurrency,
          SelfContribution: morgtSelfAmount, // Mortgage self contribution amount
          TempBridgeLoanAmt: bridgeLoanAmount, // bridge Loan amount
          TempDownPaymentAmt: downpaymentAmount, // down payment amount
          TempLoanAmt: mortgageLoanAmount, // Mortgage Loan amount
        },
        ProductDetails: {
          ProductPurpose: loanData.loanPurpose,
          ProductID: loanData.loanProductType,
          pyNote: loanData.productScope,
        },
        PropertyDetails: {
          CollateralPrice: (propData.propertyPrice)*0.75,
          CurrencyCodesList: propData.propertyCurrency,
          HasExistingProperty: propData.exsitingPropertyChecked,
          PropertyPrice: propData.propertyPrice,
          PropertyStatus: propData.propertyStatus,
          Address: {
            AddressLine1: propData.addressLine1,
            AddressLine2: propData.addressLine2,
            City: propData.cityName,
            Country: propData.countryName,
            ZipCode: propData.zipCode,
          },
        },
        pyWorkParty: {
          CoApplicant: {
            CifNbr: '890102-3287',
            DateOfBirth: '19800115',
            FirstName: coappCheckFirstName,
            FullName: coappCheckFirstName + ' ' + coappCheckLastName,
            LastName: coappCheckLastName,
            pxPartyRole: 'CoApplicant',
            pxSubscript: 'CoApplicant',
            pyEmail1: coappCheckEmailAddress,
            pyLabel: ' ',
            pyMobilePhone: coappCheckContactNumber,
            pyWorkPartyUri: '890102-3287',
            pzIndexOwnerKey: 'this.caseId',
            SumLiabilities: sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.coapplicant_liabilities), //sum of LiabilitiesList.liabilitiesAmount
            TotalIncome: '0.00',
            TotalOtherSourceIncome: sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(finanData.coapplicant_monthlies), //sum of IncomeList.IncomeValue
            pxObjClass: 'CG-Data-Party-Ind',
            Address: {
              Home: {
                AddressLine1: coappCheckAddressLine1,
                AddressLine2: coappCheckAddressLine2,
                City: coappCheckCityName,
                Country: coappCheckCountryName,
                ZipCode: coappCheckZipCode,
              },
              Work: {
                City: 'Melbourne',
                Country: 'Australia',
                StateCode: 'AL',
                ZipCode: '76142',
              },
            },
            Assets: coapplicantFinancialAssets,
            EmploymentHistory: [
              {
                EmployerName: coappCheckEmployerName,
                EmployerWorkPhone: coappCheckEmployContactNumber,
                Title: coappCheckEmployPosition,
                TypeOfEmployment: coappCheckEmployType,
                WorkStartDate: coappCheckEmployStartDate, //'20211109T050000.000 GMT'
                Address: {
                  AddressLine1: coappCheckEmployAddresLine1,
                  City: coappCheckEmployCityName,
                  Country: coappCheckEmployCountryName,
                  ZipCode: coappCheckEmployPincode,
                },
              },
            ],
            ExpensesList: coapplicantFinancialRecurring,
            Income: {
              BaseEmplIncome: coapplicantFinancialbaseIncome,
            },
            IncomeList: coapplicantFinancialMonthlySource,
            LiabilitiesList: coapplicantFinancialLiabilities,
          },
          Customer: {
            CifNbr: '8901023289',
            CustomerID: '8901023289',
            DisplayCustomerDetails: 'true',
            FirstName: borrowdata.firstName,
            KYCColour: 'Green',
            LastName: borrowdata.lastName,
            pyEmail1: borrowdata.emailAddress,
            pyMobilePhone: borrowdata.contactNumber,
            pyWorkPartyUri: 'gkuzu@yahoo.com',
            ShowPartyType: 'All party types',
            ShowStatus: 'ActiveandInActive',
            SumLiabilities: sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.liabilities), // sum of LiabilitiesList.liabilitiesAmount
            TotalIncome: '0.00',
            TotalOtherSourceIncome: sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(finanData.monthlies), //sum of IncomeList.IncomeValue
            pxObjClass: 'CG-Data-Party-Ind',
            Address: {
              Home: {
                AddressLine1: borrowdata.addressLine1,
                AddressLine2: borrowdata.addressLine2,
                City: borrowdata.cityName,
                Country: borrowdata.countryName,
                ZipCode: borrowdata.zipCode,
              },
            },
            Assets: finanData.quantities,
            EmploymentHistory: [
              {
                EmployerName: empData.employerName,
                EmployerWorkPhone: empData.contactNumber,
                Title: empData.position,
                TypeOfEmployment: empData.employeeType,
                WorkStartDate: this.formatDateGMT(empData.startDate),
                Address: {
                  AddressLine1: empData.addressLine1,
                  City: empData.cityName,
                  Country: empData.countryName,
                  ZipCode: empData.pinCode,
                },
              },
            ],
            ExpensesList: finanData.recurries,
            Income: {
              BaseEmplIncome: finanData.totalIncome,
            },
            IncomeList: finanData.monthlies,
            LiabilitiesList: finanData.liabilities,
          },
        },
      },
    };

    return formatData;
  }
  
  //========================== create case data=========================
   createcaseData(){
    let caseData = {
      
        "caseTypeID": "CG-FW-Consumer-Work-Mortgage-LoanOrigination",
        "processID": "pyStartCase",
        "parentCaseID": "",
        "content": {
          "CreationChannel": "API",
          "pyWorkParty": {
            "CoApplicant": {
              "pxObjClass": "CG-Data-Party-Ind"
            },
            "Customer": {
              "pxObjClass": "CG-Data-Party-Ind"
            }
          }
        }
      
     }
     return caseData;
   }

   //======================= change state data format==============================
   changeStateData() {
    let changedata ={
      pyAuditNote: "Proceed to next stage after update_jitendra",
      pyChangeStageOption:"goto",
      pyGotoStage: "Risk Analysis"
    }
    return changedata;
 }

 //=====================stepper validation=========================================
 onStepChange(e: any) {
  console.log(e)
 
     }


}
